﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SinglePlayerGameController : MonoBehaviour, IBtnHolder
{
    class UnActiveBtnHolder 
    {
        public Vector3 position;
        public float time;
        public UnActiveBtnHolder(Vector3 pos, float time) 
        {
            position = pos;
            this.time = time;
        }
    }

    public Transform ActiveBtn;
    public Transform UnActiveBtn;

    public float TimeToZero = 0;
    public int FailsAllowed = 0;

    Transform activeBtn;
    Transform unActiveBtn;
    BtnController activeBtnController;
    List<UnActiveBtnHolder> btnsToPlay;

    float width, height;

    UILabel levelInfoLabel;

    void Awake() 
    {
        inGame = false;
        width = Screen.width * 0.4f;
        height = Screen.height * 0.4f;

        levelInfoLabel = transform.FindChild("info").GetComponentInChildren<UILabel>();
    }

    void Start() 
    {
        activeBtn = Instantiate(ActiveBtn) as Transform;
        activeBtn.parent = transform;
        activeBtnController = activeBtn.GetComponent<BtnController>();
        activeBtnController.Holder = this;
        activeBtn.localScale = Vector3.one;

        unActiveBtn = Instantiate(UnActiveBtn) as Transform;
        unActiveBtn.parent = transform;
        unActiveBtn.localScale = Vector3.one;

        btnsToPlay = new List<UnActiveBtnHolder>();
        HidePlayBtns();
    }

    public void GeneratePlayBtns(int amount) 
    {
        for (int i = 0; i < amount; i++) 
        {
            btnsToPlay.Add(new UnActiveBtnHolder(new Vector3(Random.Range(-width, width), Random.Range(-height, height)), Random.Range(2, 7)));
        }
    }

    int curBtn;
    public void PlayGame() 
    {
        curBtn = 0;
        UpdateBtnsPos();
    }

    void UpdateBtnsPos() 
    {
        activeBtn.localPosition = btnsToPlay[curBtn].position;
        if (curBtn + 1 < btnsToPlay.Count)
        {
            unActiveBtn.localPosition = btnsToPlay[curBtn + 1].position;
        }
        else 
        {
            unActiveBtn.localPosition = Vector3.one * 1000;
        }
    }

    void HidePlayBtns() 
    {
        TimeToZero = 0;
        FailsAllowed = 0;
        failsDone = 0;
        activeBtn.localPosition = Vector3.one * 1000;
        unActiveBtn.localPosition = Vector3.one * 1000;
    }

    bool pressed;
    bool inGame;
    float pressedTime;
    public void Pressed()
    {
        activeBtnController.ExpectTime = btnsToPlay[curBtn].time;
        pressed = true;
        inGame = false;
        StartCoroutine(StartBtnCalcinTime(0.5f));
    }

    IEnumerator StartBtnCalcinTime(float time) 
    {
        yield return new WaitForSeconds(time);
        if (pressed)
        {
            activeBtnController.StartCalcing();
            pressedTime = 0;
            inGame = true;
        }
    }

    int failsDone;
    public void Unpressed()
    {
        pressed = false;
        if (inGame)
        {
            if (pressedTime < btnsToPlay[curBtn].time && pressedTime > btnsToPlay[curBtn].time - TimeToZero)
            {
                Debug.LogError(System.String.Format("in time {0:N3}", btnsToPlay[curBtn].time - pressedTime));
                pressedTime = 0;
                curBtn++;
                if (curBtn < btnsToPlay.Count)
                {
                    UpdateBtnsPos();
                }
                else
                {
                    ShowWinScreen();
                }
            }
            else
            {
                Debug.LogError(System.String.Format("fail {0:N3}", btnsToPlay[curBtn].time - pressedTime));
                failsDone++;
                if (failsDone >= FailsAllowed)
                {
                    GameOver();
                }
            }
        }
        inGame = false;
    }

    void Update() 
    {
        if (inGame) 
        {
            pressedTime += Time.deltaTime;
        }
    }

    void GameOver() 
    {
        HidePlayBtns();
        levelInfoLabel.text = "[FF0000]gameover =(";
    }

    void ShowWinScreen() 
    {
        HidePlayBtns();
        levelInfoLabel.text = "[00FF00]u win!!! =)";
    }

    public void ClearGame() 
    {
        btnsToPlay.Clear();
        levelInfoLabel.text = "";
    }

    public void BackBtnPressed() 
    {
        MainMenuController.Instance.MainMenuBtnClicked();
    }
}
