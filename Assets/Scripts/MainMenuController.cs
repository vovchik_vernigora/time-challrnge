﻿using UnityEngine;
using System.Collections;
using System.Xml;

public class MainMenuController : MonoBehaviour {

    private const string XML_URL = "http://www.scythgames.com/timechallenge.xml";

    public static MainMenuController Instance;

    public GameObject GamePanelGo;
    public GameController GameController;

    public GameObject SinglePlayerGamePanelGo;
    public SinglePlayerGameController SinglePlayerGameController;

    public static XmlDocument LoadedXML;

    public GameObject MainMenuPanelGo;
    public GameObject PlayerSelectPanelGo;

    public GameObject LevelSelectionPanelGo;

    void Awake() 
    {
        Instance = this;

        GamePanelGo = GameObject.Find("GamePanel");
        GameController = GamePanelGo.GetComponent<GameController>();

        SinglePlayerGamePanelGo = GameObject.Find("SinglePlayerGamePanel");
        SinglePlayerGameController = SinglePlayerGamePanelGo.GetComponent<SinglePlayerGameController>();

        MainMenuPanelGo = GameObject.Find("MainMenuPanel");
        PlayerSelectPanelGo = GameObject.Find("PlayerSelectPanel");

        LevelSelectionPanelGo = GameObject.Find("LevelSelection");
    }

    void Start()
    {
        MainMenuPanelGo.SetActive(true);
        PlayerSelectPanelGo.SetActive(false);
        GamePanelGo.SetActive(false);
        SinglePlayerGamePanelGo.SetActive(false);
        LevelSelectionPanelGo.SetActive(false);

        StartCoroutine(LoadWWW());
    }

    public void SinglePlayBtnClicked()
    {
        MainMenuPanelGo.SetActive(false);
        LevelSelectionPanelGo.SetActive(true);
        LevelSelection.Instance.InitLevels(GameController.Instance.EpisodeName, MainMenuController.LoadedXML.SelectNodes("Timechallenge/Episode1/Levels/*").Count);
    }

    public void PlayBtnClicked() 
    {
        MainMenuPanelGo.SetActive(false);
        PlayerSelectPanelGo.SetActive(true);
    }

    public void MainMenuBtnClicked() 
    {
        SinglePlayerGameController.ClearGame();
        SinglePlayerGamePanelGo.SetActive(false);
        MainMenuPanelGo.SetActive(true);
    }

    public void ExitBtnClicked()
    {
        Application.Quit();
    }

    public void SelectPlayerBackBtn()
    {
        MainMenuPanelGo.SetActive(true);
        PlayerSelectPanelGo.SetActive(false);
    }

    public void PlayAs1PLayer() 
    {
        LaunchGame(1);
    }
    public void PlayAs2PLayers() 
    {
        LaunchGame(2);
    }

    public void PlayAs3PLayers()
    {
        LaunchGame(3);
    }

    public void PlayAs4PLayers()
    {
        LaunchGame(4);
    }

    private void LaunchGame(int playerAmount) 
    {
        GamePanelGo.SetActive(true);
        GameController.SetPLayerAmount(playerAmount);
        GameController.StartGame();
        PlayerSelectPanelGo.SetActive(false);
    }

    IEnumerator checkInternetConnection(System.Action<bool> action)
    {
        WWW www = new WWW("http://google.com");
        yield return www;
        if (www.error != null)
        {
            action(false);
        }
        else
        {
            action(true);
        }
    }

    IEnumerator LoadWWW()
    {
        var www = new WWW(XML_URL);
        yield return www;
        var inner = www.text;
        var result = inner;
        LoadedXML = new XmlDocument();
        LoadedXML.LoadXml(result);
    }
}
