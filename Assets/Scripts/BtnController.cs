﻿using UnityEngine;
using System.Collections;

public class BtnController : MonoBehaviour {

    public IBtnHolder Holder;

    UISprite btn;

    float _expectTime = 0;
    public float ExpectTime
    {
        set 
        {
            _expectTime = value;
            if (_expectTime > 0)
            {
                txt.text = System.String.Format("{0:N3}", _expectTime);
            }
            else 
            {
                txt.text = "?.???";
            }
        }
        get 
        {
            return _expectTime;
        }
    }

    public UILabel txt;

    void Awake() 
    {
        txt = transform.GetComponentInChildren<UILabel>();
        btn = transform.GetComponentInChildren<UISprite>();
    }

    public void StartCalcing() 
    {
        calc = true;
        StartCoroutine(HideTxt());
    }

    bool calc;
    void OnPress(bool isDown)
    {
        if (isDown)
        {
            btn.color = Color.yellow;
            Holder.Pressed();
        }
        else
        {
            btn.color = Color.white;
            calc = false;
            Holder.Unpressed();
        }
    }

    IEnumerator HideTxt() 
    {
        yield return new WaitForSeconds(1);
        calc = false;
        txt.text = "";
    }

    void Update() 
    {
        if (calc && ExpectTime > 0) 
        {
            ExpectTime -= Time.deltaTime;
            txt.text = System.String.Format("{0:N3}", ExpectTime);
        }
    }
}
