﻿using UnityEngine;
using System.Collections;

public class LevelInfo : MonoBehaviour {

    string _levelName;
    public int _levelID;
    private int winpercent = 0;
    bool playable = false;
    int buttonsamount;
    float timeframe;

    public void Init(int levelID, float elHeight, float elWidth)
    {
        transform.GetComponentInChildren<UILabel>().text = (levelID + 1).ToString();
        _levelID = levelID;
        _levelName = "L" + _levelID;

        var strPrefix = "Timechallenge/" + MainMenuController.Instance.GameController.EpisodeName + "/Levels/" + _levelName;

        var buttonsamountStr = MainMenuController.LoadedXML.SelectNodes(strPrefix + "/buttonsamount")[0].InnerText;
        int.TryParse(buttonsamountStr, out buttonsamount);
        var timeframeStr = MainMenuController.LoadedXML.SelectNodes(strPrefix + "/timeframe")[0].InnerText;
        float.TryParse(timeframeStr, out timeframe);
        var winpercentStr = MainMenuController.LoadedXML.SelectNodes(strPrefix + "/winpercent");
        int.TryParse(winpercentStr[0].InnerText, out winpercent);           
        
        //string levelStatus = SetLevelImageName(_levelID);

        var level = transform.GetComponent<UISprite>();
        //level.spriteName = levelStatus;
    }

    private string SetLevelImageName(int levelID) 
    {
        string levelName = "L" + levelID;

        string result = "3";
        string clicks = "", time = "";
//        ProgressManager.Instance.GetCurrentEpisodeData(ProgressManager.CLICKS, levelName, out clicks);
//        ProgressManager.Instance.GetCurrentEpisodeData(ProgressManager.TIME, levelName, out time);
        if (!clicks.Equals("") && !time.Equals(""))
        {
            int cl, amountOfStars;
            float t;
            if (int.TryParse(clicks, out cl))
                if (float.TryParse(time, out t))
                {
                    amountOfStars = 1;
                    //if (cl < rules.clicksAllowed) { amountOfStars++; }
                    //if (t < rules.timeToFinish) { amountOfStars++; }

                    result = "playbtn" + amountOfStars;

                    playable = true;
                }
        }
        else
        {
//            ProgressManager.Instance.GetCurrentEpisodeData(ProgressManager.CLICKS, "L" + (_levelID-1), out clicks);
            if ((!clicks.Equals("")) || _levelID == 0)
            {
                result = "playbtn0";
                playable = true;
            }
        }

        return result;
    }

    public void OnClick() 
    {
        MainMenuController.Instance.SinglePlayerGamePanelGo.SetActive(true);
        MainMenuController.Instance.SinglePlayerGameController.GeneratePlayBtns(buttonsamount);
        MainMenuController.Instance.SinglePlayerGameController.TimeToZero = timeframe;
        MainMenuController.Instance.SinglePlayerGameController.FailsAllowed = buttonsamount - winpercent;
        MainMenuController.Instance.SinglePlayerGameController.PlayGame();
        MainMenuController.Instance.LevelSelectionPanelGo.SetActive(false);
    }
}
