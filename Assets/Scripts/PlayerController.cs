﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class PlayerController : MonoBehaviour, IBtnHolder
{

    int ID;

    const string RED = "[FF0000]";
    const string GREEN = "[00FF00]";
    const string YELLOW = "[FFFF00]";

    BtnController _btn;
    List<RoundResult> results;
    UILabel Label;
    UILabel WinnerLabel;
    UILabel WinnerLabelPopup;

    public int Wins;

    void Awake() 
    {
        _btn = transform.FindChild("btn").GetComponentInChildren<BtnController>();
        var g = transform.FindChild("game results");

        ID = int.Parse(gameObject.name);

        results = new List<RoundResult>();

        for(int i = 0; i < g.childCount; i++)
        {
            results.Add(g.GetChild(i).GetComponent<RoundResult>());
        }

        Label = transform.FindChild("Label").GetComponentInChildren<UILabel>();
        WinnerLabel = transform.FindChild("Winner").GetComponentInChildren<UILabel>();
        WinnerLabelPopup = transform.FindChild("WinnerPopup").GetComponentInChildren<UILabel>();
        WinnerLabelPopup.alpha = 0;
    }

    void Start() 
    {
        MainMenuController.Instance.GameController.StartGameAction += OnStartGame;
        MainMenuController.Instance.GameController.ShowTime += OnShowTime;
        MainMenuController.Instance.GameController.HideTime += OnHideTime;
        _btn.Holder = this;
    }

    public void ClearResult()
    {
        foreach (var r in results) 
        {
            r.Text = string.Empty;
            r.Visibe = false;
        }
    }

    public float TimeToPress;
    float _pressTime;
    bool culcTime = false;
    public void Pressed() 
    {
        GameController.Instance.OnBtnDown(ID);
    }

    public void ClearData() 
    {
        Wins = 0;
        ClearResult();
        _btn.txt.text = string.Empty;
        WinnerLabel.text = string.Empty;
    }

    public float prevLevelDelta;
    public void Unpressed() 
    {
        if (inGame)
        {
            Debug.LogError("Unpressed " + inGame + ID);
            culcTime = false;
            inGame = false;
            prevLevelDelta = Mathf.Abs(TimeToPress) - _pressTime;
            GameController.Instance.OnRoundFinished(ID);
        }
        else 
        {
            GameController.Instance.OnBtnUp(ID);
        }
    }

    public void OnHideTime()
    {
        _btn.txt.text = string.Empty;
    }

    public void OnShowTime() 
    {
        _btn.ExpectTime = TimeToPress;
    }

    bool inGame;
    public void OnStartGame() 
    {
        if (gameObject.activeInHierarchy)
        {
            Debug.LogError("OnStartGame " + ID);
            inGame = true;
            _btn.StartCalcing();
            _pressTime = 0;
            culcTime = true;
        }
    }

    string prefix;
    public void SetPrevLevelResult(int levelNumber,bool isWinner) 
    {
        results[levelNumber - 1].Visibe = true;
        if (isWinner)
        {
            Wins++;
            results[levelNumber - 1].BGColor = Color.green;
            prefix = GREEN;
            WinnerLabelPopup.alpha = 1;
        }
        else
        {
            if (prevLevelDelta >= 0)
            {
                prefix = YELLOW;
                results[levelNumber - 1].BGColor = Color.yellow;
            }
            else 
            {
                prefix = RED;
                results[levelNumber - 1].BGColor = Color.red;
            }
        }

        results[levelNumber - 1].Text = string.Format(prefix + "{0:N3}", Math.Round(prevLevelDelta,3));
    }

    public void ShowWinner(bool isWinner) 
    {
        if (isWinner)
        {
            WinnerLabel.text = "WINNER!!!";
        }
    }

    public void ShowDraw() 
    {
        WinnerLabel.text = YELLOW + "Draw";
    }

    public void StartNewRound(float timeToPress) 
    {
        TimeToPress = timeToPress;
    }

    void Update () 
    {
        if (culcTime) 
        {
            _pressTime += Time.deltaTime;
        }
        if (WinnerLabelPopup.alpha > 0) 
        {
            WinnerLabelPopup.alpha -= Time.deltaTime;
        }
	}
}
