﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelSelection : MonoBehaviour {

    public static LevelSelection Instance;

    public GameObject LevelCardPrefab;

    private float elHeight;
    private float elWidth;

    private Transform cardsHolder;

    private float delta;
    private Vector3 wantedPos;

    //private UILabel scoreLabel;
    //private UILabel totalScoreLabel;

    void Awake() 
    {
        Instance = this;

        int height = 2;
        int width = 5;
        elHeight = Screen.height * ((100f - 10f * 2 - ((float)height - 1) * 5f) / (float)height / 100f); // 10% gap from all borders. and 5% gap between cards
        elWidth = Screen.width * ((100f - 10f * 2 - ((float)width - 1) * 5f) / (float)width / 100f);

        cardsHolder = transform.FindChild("Levels");
        //scoreLabel = transform.FindChild("Score").GetComponent<UILabel>();
        //totalScoreLabel = transform.FindChild("totalScoreLabel").GetComponent<UILabel>();
    }

    public void UpdateScoresLabel() 
    {
        //scoreLabel.text = PlayerPrefs.GetInt(ProgressManager.SCORE, 0) + "/90";
        //totalScoreLabel.text = PlayerPrefs.GetInt(ProgressManager.TOTAL_SCORE, 0).ToString();
    }

    string curEpisodeName = "";
    public List<LevelInfo> curLevels;

    public void ReInitLevels() 
    {
        InitLevels(curEpisodeName, curLevels.Count);
    }

    public void InitLevels(string episodeName, int count) 
    {
        //STUB
        if (curLevels!= null && curLevels.Count > 0) 
        {
            foreach (var level in curLevels) 
            {
                Destroy(level.gameObject);
            }
        }
        //if (!curEpisodeName.Equals(episodeName))
        {
            curEpisodeName = episodeName;
            curLevels = new List<LevelInfo>();
            for (int j = 0; j < count; j++)
            {
                var card = (GameObject)Instantiate(LevelCardPrefab);

                var component = card.GetComponent<LevelInfo>();
                component.Init(j, (j / 5), (j % 5));
                curLevels.Add(component);

                var cTrans = card.transform;
                cTrans.parent = cardsHolder;
                var widget = card.GetComponent<UIWidget>();

                var sideSize = elWidth > elHeight ? elHeight : elWidth;

                widget.width = (int)sideSize;
                widget.height = (int)sideSize;
                widget.GetComponent<BoxCollider>().size = new Vector3(sideSize, sideSize);

                cTrans.localScale = Vector3.one;
                cTrans.localPosition = new Vector3(Screen.width * 0.05f * ((j % 5) + 2) + elWidth * (j % 5) + elWidth / 2 - Screen.width / 2,
                                                    Screen.height * 0.05f * ((j / 5) + 2) + elHeight * (1 - j / 5) + elHeight / 2 - Screen.height / 2);
            }
        }
        UpdateScoresLabel();
    }

    public void ClearPrefs() 
    {
        PlayerPrefs.DeleteAll();
    }
}
