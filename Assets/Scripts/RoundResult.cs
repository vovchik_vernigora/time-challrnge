﻿using UnityEngine;
using System.Collections;

public class RoundResult : MonoBehaviour {

    UILabel label;
    UISprite bg;

    void Awake() 
    {
        label = transform.GetComponent<UILabel>();
        bg = transform.GetComponentInChildren<UISprite>();
    }

    public string Text 
    {
        get 
        {
            return label.text;
        }
        set 
        {
            label.text = value;
        }
    }

    public string BG 
    {
        get
        {
            return bg.spriteName;
        }
        set
        {
            bg.spriteName = value;
        }
    }

    public Color BGColor
    {
        get
        {
            return bg.color;
        }
        set
        {
            bg.color = value;
        }
    }

    public bool Visibe 
    {
        get
        {
            return gameObject.activeInHierarchy;
        }
        set
        {
            gameObject.SetActive(value);
        }
    }
}
