﻿using UnityEngine;
using System.Collections;
using System;

public class GameController : MonoBehaviour {

    public static GameController Instance;

    public string EpisodeName = "Episode1";

    PlayerController[] Players;
    int[] PlayersRounds;
    int[] PlayersFinished;
    UILabel timer;

    public int Rounds = 5;

    public Action HideTime;
    public Action ShowTime;
    public Action StartGameAction;

    void Awake() 
    {
        Instance = this;

        Players = transform.FindChild("Players").GetComponentsInChildren<PlayerController>();
        timer = transform.FindChild("Timer").GetComponent<UILabel>();
    }

    int _totalAmount;
    public void SetPLayerAmount(int amount) 
    {
        _totalAmount = amount;
        PlayersRounds = new int[_totalAmount];
        PlayersFinished = new int[_totalAmount];
        PlayerTimesForThisRound = new int[_totalAmount];
        for (int i = 0; i < Players.Length; i++) 
        {
            if (i >= amount)
            {
                Players[i].gameObject.SetActive(false);
            }
            else 
            {
                Players[i].gameObject.SetActive(true);
            }
            Players[i].ClearData();
        }
    }

    public void StartGame() 
    {
        _roundNumber = 1;
        clearPlayersRounds();
        canClick = true;
        NextRound();
    }

    void clearPlayersRounds() 
    {
        for (int i = 0; i < PlayersRounds.Length; i++ )
        {
            PlayersRounds[i] = 0;
            PlayersFinished[i] = 0;
            PlayerTimesForThisRound[i] = 0;
        }
    }

    bool ValuesInArrayEqual(int[] array,int _roundNumber) 
    {
        bool result = true;

        for (int i = 0; i < array.Length; i++) 
        {
            if (array[i] != _roundNumber) 
            {
                result = false;
            }
        }
        return result;
    }

    public void OnBtnDown(int btnId)
    {
        PlayersRounds[btnId] = _roundNumber; 
        if (ValuesInArrayEqual(PlayersRounds, _roundNumber) && canClick && !levelStarted)
        {
            levelStarted = true;
            StartCoroutine(DecreaseTimer());
        }
    }

    public void OnBtnUp(int btnId)
    {
        PlayersRounds[btnId] = _roundNumber - 1;
        levelStarted = false;
        if (ValuesInArrayEqual(PlayersFinished, _roundNumber - 1))
        {
            canClick = true;
        }
    }

    bool canClick;
    bool levelStarted;
    IEnumerator DecreaseTimer() 
    {
        canClick = false;
        Debug.LogError("canClick " + canClick);
        timer.text = "GO!";
        yield return new WaitForSeconds(0.5f);
        if (levelStarted){
            ShowTime(); 
            yield return new WaitForSeconds(0.5f);
            timer.text = "";
            if (levelStarted)
            {
                BGGameSound.Instance.PlayBeep();
                BGGameSound.Instance.Play();
                Debug.LogError("round sarted " + _roundNumber);
                _roundNumber++;
                StartGameAction();
            }
            else
            {
                HideTime();
            }
        }
        timer.text = "";

        canClick = true;
    }
    
    public void OnRoundFinished(int btnId) 
    {
        PlayersFinished[btnId] = _roundNumber;

        if (ValuesInArrayEqual(PlayersFinished, _roundNumber))
        {
            canClick = true;
           levelStarted = false;
            Debug.LogError("canClick " + canClick);
            if (_roundNumber > Rounds) 
            {
                FinishGame();
            }
            else 
            {
                NextRound();
            }
        }
    }

    bool someIsEqual(int time) 
    {
        bool result = false;

        for (int i = 0; i < Players.Length; i++)
        {
            if (Players[i].TimeToPress == time)
            {
                result = true;
            }
        }
        return result;
    }

    int[] PlayerTimesForThisRound;
    int _roundNumber;
    public void NextRound() 
    {
        if (_roundNumber == 1)
        {
            for (int i = 0; i < _totalAmount; i++)
            {
                int time = UnityEngine.Random.Range(2, 7);
                PlayerTimesForThisRound[i] = time;
                Players[i].StartNewRound(time);
            }
        }
        else 
        {
            int winnerId = GetRoundWinnerId();

            for (int i = 0; i < _totalAmount; i++)
            {
                Players[i].SetPrevLevelResult(_roundNumber - 1, winnerId == i);
                int time = UnityEngine.Random.Range(2, 7);
                PlayerTimesForThisRound[i] = time;
                Players[i].StartNewRound(time);
            }
        }
        //Check if some are equal
        for (int i = 0; i < _totalAmount; i++)
        {
            for (int j = i; j < _totalAmount; j++)
            {
                if (PlayerTimesForThisRound[i] == PlayerTimesForThisRound[j]) 
                {
                    Players[i].TimeToPress = -Players[i].TimeToPress;
                    Players[j].TimeToPress = -Players[j].TimeToPress;
                }
            }
        }
    }

    int GetRoundWinnerId() 
    {
        int winnerId = -1;
        float bestTime = float.MaxValue;
        for (int i = 0; i < _totalAmount; i++)
        {
            if (Players[i].prevLevelDelta < bestTime && Players[i].prevLevelDelta > 0)
            {
                bestTime = Players[i].prevLevelDelta;
                winnerId = i;
            }
        }
        return winnerId;
    }

    int maxWins;
    public int winnerNumber;

    void FinishGame() 
    {
        maxWins = 0;
        winnerNumber = -1;

        int winnerId = GetRoundWinnerId();
        for (int i = 0; i < _totalAmount; i++)
        {
            Players[i].SetPrevLevelResult(_roundNumber - 1, winnerId == i);
        }

        if (!WinAmountEqual())
        {
            for (int i = 0; i < _totalAmount; i++)
            {
                if (maxWins < Players[i].Wins)
                {
                    maxWins = Players[i].Wins;
                    winnerNumber = i;
                }
            }

            Players[winnerNumber].ShowWinner(true);
            timer.text = "winner is player " + (winnerNumber + 1);
        }
        else
        {
            for (int i = 0; i < _totalAmount; i++)
            {
                Players[i].ShowDraw();
            }
            timer.text = "Draw";
        }
        canClick = true;
        BGGameSound.Instance.Stop();
    }

    bool WinAmountEqual() 
    {
        for (int i = 1; i < _totalAmount; i++)
        {
            if (Players[0].Wins == Players[i].Wins)
            {
                return true;
            }
        }
        return false;
    }

    public void MainMenuBtnClicked() 
    {
        MainMenuController.Instance.GamePanelGo.SetActive(false);
        MainMenuController.Instance.MainMenuPanelGo.SetActive(true);
    }

    public void ReplayBtnClicked()
    {
        SetPLayerAmount(_totalAmount);
        StartGame();
    }
}
