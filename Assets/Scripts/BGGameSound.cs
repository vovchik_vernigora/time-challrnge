﻿using UnityEngine;
using System.Collections;

public class BGGameSound : MonoBehaviour {

    public static BGGameSound Instance;

    public AudioClip BeepSound;

    AudioSource source;
    void Awake() 
    {
        source = transform.GetComponent<AudioSource>();
        Instance = this;
    }

    public void PlayBeep()
    {
        source.PlayOneShot(BeepSound);
    }

    public void Play()
    {
        if (!source.isPlaying)
        {
            source.Play();
        }
    }

    public void Stop() 
    {
        source.Stop();
    }
}
